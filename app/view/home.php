<div id="wlcSection" class="resSection">
	<div class="row">
		<h2>WELCOME</h2>
		<h1><?php $this->info("company_name"); ?></h1>
		<p><span><?php $this->info("company_name"); ?> </span> has been in business since 2004 and The company executive has over 36+ years experience in the construction industry. We are native to Florida, and a well known and well respected shell contracting company.</p>
		<a href="<?php echo URL ?>about#content" class="btn">learn more</a>
	</div>
</div>
<div id="svcSection">
	<div class="row">
		<h1>Featured Services</h1>
		<div class="services resSection">
			<dl>
				<dt> <img src="public/images/content/service1.jpg" alt="Image Service"> </dt>
				<dd>RESIDENTIAL <br> NEW CONSTRUCTION</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service2.jpg" alt="Image Service"> </dt>
				<dd>COMMERCIAL <br> NEW CONSTRUCTION</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service3.jpg" alt="Image Service"> </dt>
				<dd>COMMERCIAL BUILD OUT OR STOREFRONTS</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service4.jpg" alt="Image Service"> </dt>
				<dd>COMMERCIAL BUILD OUT FOR FACADES</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service5.jpg" alt="Image Service"> </dt>
				<dd>ADA MODIFICATION CONSTRUCTION</dd>
			</dl>
		</div>
	</div>
</div>
<div id="abtSection" class="resSection">
	<div class="row">
		<img src="public/images/content/house.jpg" alt="house" class="house resImage">
		<h2>ABOUT</h2>
		<h1><?php $this->info("company_name"); ?></h1>
		<p>Our goal is to deliver exceptional craftsmanship, precise project management with unsurpassed customer service, delivering the results of your dreams - on time and on budget.</p>
		<p>We approach each project with a cooperative mindset, working with clients, architects, and subcontractors toward the common goal – successful project delivery. Our diverse construction portfolio ensures that each project is matched with appropriate resources and expertise.</p>
		<p>Through technical skill and pre-construction know-how, we anticipate project challenges, develop solutions that meet client objectives, and deliver top-notch projects.</p>
		<p>Your project will receive professional supervision, attention to detail, and design input rarely found in our field. We bring a common sense approach to sustainability, focusing on getting you the most benefits at little or no additional cost.</p>
	</div>
</div>
<div id="rvwSection" class="resSection">
	<div class="row">
		<p>“</p>
		<p>As with other projects, you came through for us by completing these projects within tight timelines and budgets. Your employees were professional and respectful of the need to do a quality job while not impacting our ability to do business.</p>
		<p>Personally, I appreciate that you make yourself available when requested, you are timely with returning phone calls and emails, and I can always trust you to be fair and honest. The values you demonstrate as the head of your company are seen in your employees. Kwoin Construction Corp. has made us feel like a highly valued customer and we will certainly contact you for bids on future work, as well as continue to recommend you to others.</p>
		<img src="public/images/content/profile-sprites.png" alt="profiles">
		<p>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</p>
		<p>- BECKER S.</p>
	</div>
</div>
<div id="gallSection" class="resSection">
	<div class="row">
		<h1>Our Gallery</h1>
		<div class="gallery">
			<img src="public/images/content/gallery1.jpg" alt="Gallery Image">
			<img src="public/images/content/gallery2.jpg" alt="Gallery Image">
			<img src="public/images/content/gallery3.jpg" alt="Gallery Image">
			<img src="public/images/content/gallery4.jpg" alt="Gallery Image">
			<img src="public/images/content/gallery5.jpg" alt="Gallery Image">
			<img src="public/images/content/gallery6.jpg" alt="Gallery Image">
		</div>
		<img src="public/images/content/imgBot.jpg" alt="planner" class="imgBot resImage">
	</div>
</div>
<div id="cntSection" class="resSection">
	<div class="row">
		<h1>Contact Us</h1>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="cntLeft col">
				<div class="cnt2Left col fl">
					<input type="text" name="name" placeholder="name:">
					<input type="text" name="phone" placeholder="phone:">
				</div>
				<div class="cnt2Right col fr">
					<input type="text" name="email" placeholder="email:">
					<input type="text" name="subject" placeholder="subject:">
				</div>
				<div class="clearfix"></div>
				<textarea name="message" cols="30" rows="10" placeholder="comments"></textarea>
			</div>
			<div class="cntRight col">
				<label>
					<input type="checkbox" name="consent" class="consentBox"><p class="conset">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</p>
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> <p class="conset">I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></p>
				</label>
				<?php endif ?>
				<div class="g-recaptcha"></div>
				<button type="submit" class="ctcBtn btn" disabled>SUBMIT</button>
			</div>
		</form>
	</div>
</div>
<div id="cntDetContact" class="resSection">
	<div class="row">
			<div class="details"><img src="public/images/sprite.png" alt="logo" class="bg-location"><p><?php $this->info("address"); ?></p></div>
			<div class="details"><img src="public/images/sprite.png" alt="logo" class="bg-phone"><p><?php $this->info(["phone","tel"]); ?></p></div>
			<div class="details"><img src="public/images/sprite.png" alt="logo" class="bg-email"><p><?php $this->info(["email","mailto"]); ?></p></div>
	</div>
</div>
